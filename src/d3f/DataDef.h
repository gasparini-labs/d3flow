#pragma once

// D3F
#include <d3f/Datatypes.h>

namespace d3f {

   class DataDef
   {
   public:
      DataDef();

      virtual ~DataDef();

      /// \brief Identify a domain indexed by 'ids'.
      /// \param ids The indexes that identifies the domain in its parent object.
      /// \return Self reference for fluent interface.
      DataDef& operator()( const ids_t &ids );

      /// \brief Identify a temporal version of a domain (previously identified).
      /// \param it The temporal index. Temporal indexes comes from loop indexes.
      /// \return Self reference for fluent interface.
      DataDef& operator[]( it_t it );

   private:
//      enum States : sm_t { DDS_DEF=0, DDS_DONE, DDS_ERROR };
//      enum Events : sm_t { DDE_DEF=0, DDE_DONE };
//      StateMachine<3,2> m_sm;
   };

}
