// D3F
#include <d3f/TaskDef.h>

namespace d3f {

   TaskDef::TaskDef() :
      m_key( "" ),
      m_ids( {} ),
      m_puOwner( PU_UNKNOWN )
   {
   }

   TaskDef::~TaskDef()
   {
   }

   TaskDef& TaskDef::in( d3f::dkey_t domKey, const ids_t &ids, d3f::state_t inState )
   {
      return *this;
   }

   TaskDef& TaskDef::out( d3f::dkey_t domKey, const ids_t &ids, d3f::state_t outState )
   {
      return *this;
   }

   TaskDef& TaskDef::inout( d3f::dkey_t domKey, const ids_t &ids, d3f::state_t inState, d3f::state_t outState )
   {
      return *this;
   }

   void TaskDef::end()
   {
   }

}
