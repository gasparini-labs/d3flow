#pragma once

// STD
#include <memory>
#include <string>

// D3F
#include <d3f/Object.h>
#include <d3f/Runtime.h>
#include <d3f/TaskDef.h>

namespace d3f {

   class D3Flow
   {
   public:
      D3Flow();

      virtual ~D3Flow();

      D3Flow& object( const std::shared_ptr<Object> &object ) {}

      D3Flow& task( const std::shared_ptr<TaskDef> &task ) {}

      template<typename F_RUNTIME>
      void execute( const F_RUNTIME &fRuntime )
      {
         Runtime rt{ this };
         fRuntime( rt );
      }
   private:
      //const std::shared_ptr<ProcessingEnvironment> m_processingEnvironment;
      const std::string m_name;
   };

}

