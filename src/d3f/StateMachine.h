#pragma once

// STD
#include <array>
#include <vector>

// D3F
#include <d3f/Asserts.h>

namespace d3f {

   using sm_t = unsigned short int;
   using even_t = unsigned short int;

   template<sm_t NS>
   using states_t = std::array<sm_t,NS>;

   template<sm_t NE>
   using events_t = std::array<sm_t,NE>;

   template<sm_t NS, sm_t NE>
   using rules_t = std::array< std::array<sm_t,NE> ,NS >;

   template<sm_t NS, sm_t NE>
   class StateMachine
   {
   public:
      StateMachine( const rules_t<NS,NE> &rules ) :
         m_rules( rules ),
         m_state( 0 )
      {
         static_assert( NS > 0 );
         static_assert( NE > 0 );
      }

      sm_t state() const
      {
         return m_state;
      }

      sm_t update( sm_t event )
      {
         dck_lss( event, NE );
         return ( m_state = m_rules[ m_state ][ event ] );
      }
   private:
      const rules_t<NS,NE> m_rules;
      sm_t m_state;
   };

}
