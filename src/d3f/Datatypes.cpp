// D3F
#include <d3f/Asserts.h>
#include <d3f/Datatypes.h>

namespace d3f {

   template<typename T>
   static int Init_size( const T& ids )
   {
      return ids.size();
   }

   template<typename T>
   static std::shared_ptr<id_t[]> Init_ids( const T& ids )
   {
      auto const arraySize = ids.size();
      auto array = std::make_shared<id_t[]>( arraySize );
      std::copy( ids.begin(), ids.end(), array.get() );
      return array;
   }

   ids_t::ids_t( const std::initializer_list<id_t>& ids ) :
      m_size( Init_size( ids ) ),
      m_ids(  Init_ids( ids ) )
   {}

   ids_t::ids_t( const std::vector<id_t>& ids ) :
      m_size( Init_size( ids ) ),
      m_ids(  Init_ids( ids ) )
   {}

   ids_t::ids_t( ids_t&& other ) :
      m_size( other.m_size ),
      m_ids( std::move( other.m_ids ) )
   {}

   ids_t::ids_t( const ids_t& other ) :
      m_size( other.m_size ),
      m_ids( other.m_ids ) // It may cause problems, because "*this" and "other" will share the same indexes.
   {}

   ids_t& ids_t::operator=( ids_t&& other )
   {
      dck_equ( m_size, other.m_size );
      m_ids = std::move( other.m_ids );
      return *this;
   }

   ids_t& ids_t::operator=( const ids_t& other )
   {
      dck_equ( m_size, other.m_size );
      m_ids = other.m_ids; // It may cause problems, because "*this" and "other" will share the same indexes.
      return *this;
   }

   ids_t ids_t::sub( id_t first ) const
   {
      // { a, b, c }
      // sub( 0 ) -> { a, b, c }
      // sub( 1 ) -> { b, c }
      dck_bet( first, 0, size() );
      int const sz = size() - first;
      ids_t _sub = Origin( sz );
      for ( id_t k1 = first, k2 = 0; k2 < sz; ++k1, ++k2 )
         _sub[ k2 ] = m_ids[ k1 ];
      return _sub;
   }

   ids_t ids_t::sub( id_t first, int sz ) const
   {
      // { a, b, c }
      // sub( 0, 3 ) -> { a, b, c }
      // sub( 0, 2 ) -> { a, b }
      dck_bet( sz, 0, size() );
      dck_bet( first, 0, size() );
      dck_leq( first + sz, size() );
      ids_t _sub = Origin( sz );
      for ( id_t k1 = first, k2 = 0; k2 < sz; ++k1, ++k2 )
         _sub[ k2 ] = m_ids[ k1 ];
      return _sub;
   }

   ids_t ids_t::concat( const ids_t& other ) const
   {
      int const sz = size() + other.size();
      ids_t _sub = Origin( sz );
      for ( id_t k1 = 0; k1 < size(); ++k1 )
         _sub[ k1 ] = m_ids[ k1 ];
      for ( id_t k2 = 0; k2 < other.size(); ++k2 )
         _sub[ size() + k2 ] = other.m_ids[ k2 ];
      return _sub;
   }

   bool operator==( const ids_t& lhs, const ids_t& rhs )
   {
      dck_equ( lhs.size(), rhs.size() );
      bool equals = lhs.size() == rhs.size();
      for ( int k = 0; k < lhs.size(); ++k )
         equals = equals && ( lhs[ k ] == rhs[ k ] );
      return equals;
   }

   bool operator!=( const ids_t& lhs, const ids_t& rhs )
   {
      return !( lhs == rhs );
   }

   bool operator<( const ids_t& lhs, const ids_t& rhs )
   {
      dck_equ( lhs.size(), rhs.size() );
      bool res = false;
      for ( int k = 0; k < lhs.size(); ++k )
         if ( lhs[ k ] != rhs[ k ] )
         {
            res = lhs[ k ] < rhs[ k ];
            break;
         }
      return res;
   }

   bool operator<=( const ids_t& lhs, const ids_t& rhs )
   {
      dck_equ( lhs.size(), rhs.size() );
      bool res = true;
      for ( int k = 0; k < lhs.size(); ++k )
         if ( lhs[ k ] != rhs[ k ] )
         {
            res = lhs[ k ] < rhs[ k ];
            break;
         }
      return res;
   }

   bool operator>( const ids_t& lhs, const ids_t& rhs )
   {
      dck_equ( lhs.size(), rhs.size() );
      bool res = false;
      for ( int k = 0; k < lhs.size(); ++k )
         if ( lhs[ k ] != rhs[ k ] )
         {
            res = lhs[ k ] > rhs[ k ];
            break;
         }
      return res;
   }

   bool operator>=( const ids_t& lhs, const ids_t& rhs )
   {
      dck_equ( lhs.size(), rhs.size() );
      bool res = true;
      for ( int k = 0; k < lhs.size(); ++k )
         if ( lhs[ k ] != rhs[ k ] )
         {
            res = lhs[ k ] > rhs[ k ];
            break;
         }
      return res;
   }

   static void Test()
   {
      /// puProcess
      /// puThread
      /// puAccelerator
      ///
      /// {puProcess}
      /// {0}
      /// {1}
      /// {ALL} --> process collective
      ///
      /// {puProcess,puThread}
      /// {p,0}
      /// {p,1}
      /// {p,ANY}
      /// {p,ALL} --> thread collective
      /// {ALL,ALL} --> process/thread collective
      ///
      ///
      /// {puProcess,puAccelerator}
      ///
      ///
      ///
      /// {puProcess,puThread,puAccelerator}
      /// {p,0,a}
      /// {p,ANY,a}
      ///
      /// rtm.task( "y = Ax" )
      ///    .in( A{}, 0 )
      ///    .in( A{i,j}, 0 )
      ///
   }

}
