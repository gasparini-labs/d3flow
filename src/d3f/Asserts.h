#pragma once

// STD
#include <exception>
#include <string>
#include <typeinfo>

// D3F
#include <d3f/Error.h>

namespace d3f::ck {

   static inline std::string location( const char *file, int line )
   {
      return "[" + std::string( file ) + ":" + std::to_string( line ) + "]";
   }

   static inline std::string location( const char *file, int line, const std::string& msg )
   {
      return "[" + std::string( file ) + ":" + std::to_string( line ) + "]\n" + msg;
   }

   template<typename X>
   static inline std::string to_string( X &&x, const char *xName )
   {
      if constexpr ( std::is_same< bool, typename std::decay<X>::type >::value )
         return std::string( x ? "true" : "false" );
      else if constexpr ( std::is_arithmetic< typename std::decay<X>::type >::value )
         return std::to_string( x );
      else
         return std::string( xName );
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X equ( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x == y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X neq( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x != y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X lss( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x < y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X leq( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x <= y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X gtr( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x > y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X geq( X x, Y y, const F_ERROR &fError )
   {
      if ( !( x >= y ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename Z, typename F_ERROR>
   static inline X bet( X x, Y y, Z z, const F_ERROR &fError )
   {
      if ( !( ( x >= y ) && ( x < z ) ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename F_ERROR>
   static inline X is_true( X x, const F_ERROR &fError )
   {
      if ( !( (bool) x ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename F_ERROR>
   static inline X is_false( X x, const F_ERROR &fError )
   {
      if ( ( (bool) x ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X logic_or( X x, Y y, const F_ERROR &fError )
   {
      if ( !( static_cast<bool>( x ) || static_cast<bool>( y ) ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X logic_xor( X x, Y y, const F_ERROR &fError )
   {
      if ( !( static_cast<bool>( x ) ? !static_cast<bool>( y ) : static_cast<bool>( y ) ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X logic_and( X x, Y y, const F_ERROR &fError )
   {
      if ( !( static_cast<bool>( x ) && static_cast<bool>( y ) ) )
         throw Error( fError() );
      return x;
   }

   template<typename X, typename Y, typename F_ERROR>
   static inline X implies( X x, Y y, const F_ERROR &fError )
   {
      if ( !( !static_cast<bool>( x ) || static_cast<bool>( y ) ) )
         throw Error( fError() );
      return x;
   }

}

/// \brief Assert that 'x == y'
#define ckm_equ( x, y, msg ) d3f::ck::equ( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' == '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " == " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'NOT x == y'
#define ckm_neq( x, y, msg ) d3f::ck::neq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' != '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " != " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x < y'
#define ckm_lss( x, y, msg ) d3f::ck::lss( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' < '"  + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " < "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x <= y'
#define ckm_leq( x, y, msg ) d3f::ck::leq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' <= '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " <= " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x > y'
#define ckm_gtr( x, y, msg ) d3f::ck::gtr( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' > '"  + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " > "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x >= y'
#define ckm_geq( x, y, msg ) d3f::ck::geq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' >= '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " >= " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x between [y,z)', i.e., 'y <= x < z'
#define ckm_bet( x, y, z, msg ) d3f::ck::bet( (x), (y), (z), [&]()->std::string { return std::string( "Precondition failed: '" ) + #y + "' <= '" + #x + "' < '" + #z + "', but '" + d3f::ck::to_string( (y), #y ) + " <= " + d3f::ck::to_string( (x), #x ) + " < " + d3f::ck::to_string( (z), #z ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x' is true
#define ckm_true( x, msg )   d3f::ck::is_true( (x), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' is TRUE, but '"  + d3f::ck::to_string( (x), #x ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x' is false
#define ckm_false( x, msg ) d3f::ck::is_false( (x), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' is FALSE, but '" + d3f::ck::to_string( (x), #x ) + "' is TRUE "  + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x || y' is true
#define ckm_or( x, y, msg ) d3f::ck::logic_or(   (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' OR '"  + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " OR "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x XOR y' is true (exclusive OR)
#define ckm_xor( x, y, msg ) d3f::ck::logic_xor( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' XOR '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " XOR " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x && y' is true
#define ckm_and( x, y, msg ) d3f::ck::logic_and( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' AND '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " AND " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )
/// \brief Assert that 'x ==> y' is true (implies)
#define ckm_imp( x, y, msg ) d3f::ck::implies(   (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' ==> '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " ==> " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__, (msg) ); } )

/// \brief Assert that 'x == y'
#define ck_equ( x, y ) d3f::ck::equ( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' == '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " == " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'NOT x == y'
#define ck_neq( x, y ) d3f::ck::neq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' != '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " != " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x < y'
#define ck_lss( x, y ) d3f::ck::lss( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' < '"  + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " < "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x <= y'
#define ck_leq( x, y ) d3f::ck::leq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' <= '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " <= " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x > y'
#define ck_gtr( x, y ) d3f::ck::gtr( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' > '"  + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " > "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x >= y'
#define ck_geq( x, y ) d3f::ck::geq( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' >= '" + #y + "', but '" + d3f::ck::to_string( (x), #x ) + " >= " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x between [y,z)', i.e., 'y <= x < z'
#define ck_bet( x, y, z ) d3f::ck::bet( (x), (y), (z), [&]()->std::string { return std::string( "Precondition failed: '" ) + #y + "' <= '" + #x + "' < '" + #z + "', but '" + d3f::ck::to_string( (y), #y ) + " <= " + d3f::ck::to_string( (x), #x ) + " < " + d3f::ck::to_string( (z), #z ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x' is true
#define ck_true( x )   d3f::ck::is_true( (x), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' is TRUE, but '"  + d3f::ck::to_string( (x), #x ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x' is false
#define ck_false( x ) d3f::ck::is_false( (x), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' is FALSE, but '" + d3f::ck::to_string( (x), #x ) + "' is TRUE "  + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x || y' is true
#define ck_or( x, y ) d3f::ck::logic_or(   (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' OR '"  + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " OR "  + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x XOR y' is true (exclusive OR)
#define ck_xor( x, y ) d3f::ck::logic_xor( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' XOR '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " XOR " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x && y' is true
#define ck_and( x, y ) d3f::ck::logic_and( (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' AND '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " AND " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )
/// \brief Assert that 'x ==> y' is true (implies)
#define ck_imp( x, y ) d3f::ck::implies(   (x), (y), [&]()->std::string { return std::string( "Precondition failed: '" ) + #x + "' ==> '" + #y + "' is TRUE, but '" + d3f::ck::to_string( (x), #x ) + " ==> " + d3f::ck::to_string( (x), #y ) + "' is FALSE " + d3f::ck::location( __FILE__, __LINE__ ); } )

#if defined(NDEBUG)
#  define dckm_equ( x, y, msg )
#  define dckm_neq( x, y, msg )
#  define dckm_lss( x, y, msg )
#  define dckm_leq( x, y, msg )
#  define dckm_gtr( x, y, msg )
#  define dckm_geq( x, y, msg )
#  define dckm_bet( x, y, z, msg )
#  define dckm_true( x, msg )
#  define dckm_false( x, msg )
#  define dckm_or( x, y, msg )
#  define dckm_xor( x, y, msg )
#  define dckm_and( x, y, msg )
#  define dckm_imp( x, y, msg )
#  define dck_equ( x, y )
#  define dck_neq( x, y )
#  define dck_lss( x, y )
#  define dck_leq( x, y )
#  define dck_gtr( x, y )
#  define dck_geq( x, y )
#  define dck_bet( x, y, z )
#  define dck_true( x )
#  define dck_false( x )
#  define dck_or( x, y )
#  define dck_xor( x, y )
#  define dck_and( x, y )
#  define dck_imp( x, y )
#else
#  define dckm_equ( x, y, msg ) ckm_equ( x, y, msg )
#  define dckm_neq( x, y, msg ) ckm_neq( x, y, msg )
#  define dckm_lss( x, y, msg ) ckm_lss( x, y, msg )
#  define dckm_leq( x, y, msg ) ckm_leq( x, y, msg )
#  define dckm_gtr( x, y, msg ) ckm_gtr( x, y, msg )
#  define dckm_geq( x, y, msg ) ckm_geq( x, y, msg )
#  define dckm_bet( x, y, z, msg ) ckm_bet( x, y, z, msg )
#  define dckm_true( x, msg ) ckm_true( x, msg )
#  define dckm_false( x, msg ) ckm_false( x, msg )
#  define dckm_or( x, y, msg ) ckm_or( x, y, msg )
#  define dckm_xor( x, y, msg ) ckm_xor( x, y, msg )
#  define dckm_and( x, y, msg ) ckm_and( x, y, msg )
#  define dckm_imp( x, y, msg ) ckm_imp( x, y, msg )
#  define dck_equ( x, y ) ck_equ( x, y )
#  define dck_neq( x, y ) ck_neq( x, y )
#  define dck_lss( x, y ) ck_lss( x, y )
#  define dck_leq( x, y ) ck_leq( x, y )
#  define dck_gtr( x, y ) ck_gtr( x, y )
#  define dck_geq( x, y ) ck_geq( x, y )
#  define dck_bet( x, y, z ) ck_bet( x, y, z )
#  define dck_true( x ) ck_true( x )
#  define dck_false( x ) ck_false( x )
#  define dck_or( x, y ) ck_or( x, y )
#  define dck_xor( x, y ) ck_xor( x, y )
#  define dck_and( x, y ) ck_and( x, y )
#  define dck_imp( x, y ) ck_imp( x, y )
#endif

