#pragma once

// STD
#include <array>

// D3F
#include <d3f/D3Flow.h>
#include <d3f/Datatypes.h>
#include <d3f/TaskDef.h>

namespace d3f {

   /// \brief Forward declaration of D3Flow class
   class D3Flow;

   class Runtime
   {
   public:
      Runtime( D3Flow *d3flow );
      virtual ~Runtime();

      template<typename F_TASK>
      void task( d3f::tkey_t tkey, const F_TASK & fTask )
      {
         // get task
         // TaskDef::Builder tb( &task );
//         fTask( tb );
      }

      auto taskDefBgn( d3f::tkey_t tkey ) -> TaskDef& {}
      auto taskDefEnd() -> void {}

      TaskDef& task( d3f::tkey_t tkey );

      template<typename F_GENERATOR>
      void generator( const char *dkey, int s, const F_GENERATOR &fGenerator )
      {
      }

      void reduce();

      template<typename T, typename F_FOREACH>
      void foreach( const std::array<T,2> &range, const F_FOREACH &fForeach )
      {
      }

      template<typename T, typename F_FOREACH>
      void foreach( const std::array<T,3> &range, const F_FOREACH &fForeach )
      {
      }

      template<typename T, typename F_FOREACH>
      void foreach( T bgn, T end, T incr, const F_FOREACH &fForeach )
      {
      }

      template<typename T, typename F_FOREACH>
      void foreach( T bgn, T end, const F_FOREACH &fForeach )
      {
      }

      template<typename F_WHILE>
      void while_( bool cond, const F_WHILE &fWhile )
      {
      }

      template<typename F_TRUE>
      void if_( bool cond, const F_TRUE &fTrue )
      {
      }

      template<typename F_TRUE>
      void elsif_( bool cond, const F_TRUE &fTrue )
      {
      }

      template<typename F_ELSE>
      void else_( const F_ELSE &fElse )
      {
      }

      template<typename F_TRUE, typename F_FALSE>
      void if_else( bool cond, const F_TRUE &fTrue, const F_FALSE &fFalse = [](){} )
      {
      }

      void break_( int levels = 1 );

      void continue_();
   private:
      D3Flow *m_d3flow;
   };

}

