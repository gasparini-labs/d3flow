// D3F
#include <d3f/Runtime.h>

namespace d3f {

   Runtime::Runtime( D3Flow *d3flow ) :
      m_d3flow( d3flow )
   {
   }

   Runtime::~Runtime()
   {
   }

   void Runtime::reduce()
   {
   }

   void Runtime::break_( int levels )
   {
   }

   TaskDef& Runtime::task( d3f::tkey_t tkey )
   {
   }

   void Runtime::continue_()
   {
   }

   static void Test()
   {
      int* ia;
      int* ja;
      double* va;


   }

//   static void Test()
//   {
//      Runtime rt( nullptr );
//
//      constexpr double CONV_TOL = 1.0e-5;
//      constexpr int MAX_IT = 10;
//      double r[MAX_IT+1], x[MAX_IT+1], p[MAX_IT+1], a[MAX_IT+1], B[MAX_IT+1], Ap[MAX_IT+1], rr[MAX_IT+1];
//      double A, b;
//
//      r[0] = b - A*x[0];
//
//      rr[0] = r[0]*r[0];
//      if ( std::sqrt( rr[0] ) < CONV_TOL )
//         return;
//
//      p[0] = r[0];
//
//      rt.foreach( 0, MAX_IT, []( it_t k )
//      {
//         Ap[k] = A*p[k];
//         a[k] = rr[k] / ( p[k]*Ap[k] );
//
//         x[k+1] = x[k] + a[k]*p[k];
//         r[k+1] = r[k] - a[k]*Ap[k];
//
//         rr[k+1] = r[k+1]*r[k+1];
//         rt.if_( std::sqrt( rr[k+1] ) < CONV_TOL, [&]()
//         {
//            rt.break_();
//         } );
//
//         B[k] = rr[k+1] / rr[k];
//         p[k+1] = r[k+1] + B[k]*p[k];
//      } );
//
//   }

//   static void Residual()
//   {
//      Runtime rt( nullptr );
//      double r, b, A, x;
//      rt.put( ck(i,RemoteCoupledJs) );
//      rt.get( ck(RemoteCoupledJs,i) );
//      r(i)[0] = b(i) - A(i,LocalCoupledJs)*cx(LocalCoupledJs,i)   | rt.task( "residual local" );
//      r(i)[1] -= A(i,RemoteCoupledJs)*cx(RemoteCoupledJs,i)       | rt.task( "residual remote" );
//   }

//   static void Norm2()
//   {
//      Runtime rt( nullptr );
//      double r, rr, rri;
//      rri = r(i) >> rt.task( "local dot x" );
//      rr += rri  >> rt.task( "allReduce x" );
//   }

//   static void Test3()
//   {
//      Runtime rt( nullptr );
//
//      constexpr double CONV_TOL = 1.0e-5;
//      constexpr int MAX_IT = 10;
//      double r[MAX_IT+1], x[MAX_IT+1], p[MAX_IT+1], a[MAX_IT+1], B[MAX_IT+1], Ap[MAX_IT+1], rr[MAX_IT+1];
//      double A, b;
//      int nIter = 0;
//
//      r[0] = b - A*x[0] | rt.task( "residual" );
//      rr[0] = r[0]*r[0] | rt.task( "norm2" );
//
//      rt.if_( std::sqrt( rr[0] ) < CONV_TOL, []()
//      {
//         nIter = 0;
//         sol = x[0]     | rt.task( "assign" );
//         return;
//      } );
//
//      p[0] = r[0] | rt.task( "assign" );
//
//      rt.foreach( 0, MAX_IT, []( it_t k )
//      {
//         Ap[k] = A*p[k]                | rt.task( "matvec" );
//         a[k] = rr[k] / ( p[k]*Ap[k] ) | rt.task( "alpha" );
//
//         x[k+1] = x[k] + a[k]*p[k]     | rt.task( "update" );
//         r[k+1] = r[k] - a[k]*Ap[k]    | rt.task( "update" );
//
//         rr[k+1] = r[k+1]*r[k+1]       | rt.task( "norm2" );
//         rt.if_( std::sqrt( rr[k+1] ) < CONV_TOL, [&]()
//         {
//            nIter = k+1;
//            sol = x[k+1]               | rt.task( "assign" );
//            rt.break_();
//         } );
//
//         B[k] = rr[k+1] / rr[k]        | rt.task( "beta" );
//         p[k+1] = r[k+1] + B[k]*p[k]   | rt.task( "update" );
//      } );
//
//   }

//   static void Test2()
//   {
//      Runtime rt( nullptr );
//
//      constexpr double CONV_TOL = 1.0e-5;
//      constexpr int MAX_IT = 10;
//      double r[MAX_IT+1], x[MAX_IT+1], p[MAX_IT+1], a[MAX_IT+1], B[MAX_IT+1], Ap[MAX_IT+1], rr[MAX_IT+1];
//      double A, b, sol;
//      int Anzj;
//
//      rt.task( "residual" )
//         .in( b(i) )
//         .in( A(i,Anzj) )
//         .in( x(Anzj) )
//         .out( r(i) );
//
//      rt.task( "norm2" )
//         .in( r(i) )
//         .allReduce( rr[0] );
//
//      rt.if_( std::sqrt( rr[0] ) < CONV_TOL, [&]()
//      {
//         rt.task( "solution" )
//            .in( x(i)[0] )
//            .allReduce( sol(i) );
//         rt.exit_();
//      } );
//
//      rt.task( "assign" )
//         .in( r(i) )
//         .allReduce( p(i) );
//
//      rt.foreach( 0, MAX_IT, []( it_t k )
//      {
//         rt.task( "matvec" )
//            .in( A(i,Anzj) )
//            .in( p(Anzj)[k] )
//            .out( Ap(i)[k] );
//
//         rt.task( "alpha" )
//            .in( rr[k] )
//            .in( p(i)[k] )
//            .in( Ap(i)[k] )
//            .allReduce( a[k] );
//
//         rt.task( "update_x" )
//            .in( a[k] )
//            .in( x(i)[k] )
//            .in( p(i)[k] )
//            .out( x(i)[k+1] );
//
//         rt.task( "update_r" )
//            .in( a[k] )
//            .in( r(i)[k] )
//            .in( Ap(i)[k] )
//            .out( r(i)[k+1] );
//
//         rt.task( "norm2" )
//            .in( r(i)[k+1] )
//            .allReduce( rr[k+1] );
//
//         rt.if_( std::sqrt( rr[k+1] ) < CONV_TOL, [&]()
//         {
//            rt.task( "solution" )
//               .in( x(i)[k+1] )
//               .allReduce( sol(i) );
//            rt.break_();
//         } );
//
//         rt.task( "beta" )
//            .in( rr[k] )
//            .in( rr[k+1] )
//            .allReduce( B[k] );
//
//         rt.task( "update_p" )
//            .in( B[k] )
//            .in( p(i)[k] )
//            .in( r(i)[k+1] )
//            .out( p(i)[k+1] );
//      } );
//
//   }

}
