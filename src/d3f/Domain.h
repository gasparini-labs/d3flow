#pragma once

// STD
#include <functional>
#include <map>
#include <string>

// D3F
#include <d3f/Datatypes.h>

namespace d3f {

   enum ProcessType : int { PT_SINGLE_PROCESS=0, PT_MULTI_PROCESS };
   enum ThreadType : int { TT_SINGLE_THREAD=0, TT_MULTI_THREAD };
   enum ExecContext : int { EC_PROCESS=0, EC_THREAD };
   enum ObjectType : int { OT_PRIVATE_OBJECT=0, OT_REPLICATED_OBJECT, OT_PARTITIONED_OBJECT };

   struct ProcessContext
   {
   };

   struct ThreadContext
   {
   };

   /// \brief
   /// A          --> level=0,   exec_context=EC_PROCESS,  obj_type=OT_PARTITIONED_OBJECT    proc_owner=distributed     thrd_owner=distributed
   /// A(i,j)     --> level=1,   exec_context=EC_PROCESS,  obj_type=OT_PRIVATE_OBJECT        proc_owner=private         thrd_owner=shared
   /// A(i,j)(t)  --> level=2,   exec_context=EC_THREAD,   obj_type=OT_PRIVATE_OBJECT        proc_owner=parent          thrd_owner=private
   ///
   /// Tip: partitioned may be used as both distributed and shared (in some circumstances).
   ///
   /// A          --> level=0    exec_context=EC_PROCESS_TEAM     obj_type=OT_PARTITIONED    proc_owner=distributed     thrd_owner=distributed
   ///
   ///
   ///
   ///
   ///

   class Domain
   {
   public:
      Domain();

      virtual ~Domain();

      virtual const Domain* domain() const { return std::cref( *this ); } // Self as domain

      virtual const Domain* domain( d3f::ids_t ids ) const = 0; // Subdomains

      virtual void foreachDomain( const std::function<void(ids_t,Domain*)> &fDomains ) = 0;

      virtual void foreachDomain( const std::function<void(ids_t,const Domain*)> &fDomains ) const = 0;

      const Domain& operator()() const { return domain(); }

      const Domain& operator()( d3f::ids_t ids ) const { return domain( ids ); }

      const Domain* parent() const { return m_parent; }

      int nLevels() const { return m_nLevels; }

      int level() const { return m_level; }

      ///
      /// iterCount
   private:
      const Domain *m_parent;
      const std::string m_generalName;
      const std::string m_instanceName;
      const int m_nLevels;
      const int m_level;
      /// Owner: Execution Unit (ExecUnit)
      /// Parent --> ExecProccess
      /// Child --> ExecThread | ExecAcc
      /// Execution Unit: AllProcesses, Process, AllThreads, Thread, Accelerator
   };

}
