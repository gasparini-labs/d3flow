#pragma once

// D3F
#include <d3f/Datatypes.h>

namespace d3f {

   class Object
   {
   public:
      Object();
      virtual ~Object();

      virtual const Object& domain() const { return std::cref( *this ); } // Self as domain

      virtual const Object& domain( d3f::ids_t ids ) const; // Subdomains

      const Object& operator()() const { return domain(); }

      const Object& operator()( d3f::ids_t ids ) const { return domain( ids ); }

      const Object* parent() const { return m_parent; }

      int nLevels() const { return m_nLevels; }

      int level() const { return m_level; }

      ///
      /// iterCount
   private:
      const Object *m_parent;
      const int m_nLevels;
      const int m_level;
      /// Owner: Execution Unit (ExecUnit)
      /// Parent --> ExecProccess
      /// Child --> ExecThread | ExecAcc
      /// Execution Unit: AllProcesses, Process, AllThreads, Thread, Accelerator
      ///
      /// Process Group


   };

}

