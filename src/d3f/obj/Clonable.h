#pragma once

namespace d3f {

   class Object;

}

namespace d3f::obj {

   class Clonable
   {
   public:
      Clonable() = delete;

      virtual Object* clone() const;
   };

}
