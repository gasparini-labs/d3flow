#pragma once

// D3F
#include <d3f/Datatypes.h>

namespace d3f {

   class TaskDef
   {
   public:
      TaskDef();
      virtual ~TaskDef();
      virtual TaskDef& in( d3f::dkey_t domKey, const ids_t& ids, d3f::state_t inState );
      virtual TaskDef& out( d3f::dkey_t domKey, const ids_t& ids, d3f::state_t outState );
      virtual TaskDef& inout( d3f::dkey_t domKey, const ids_t& ids, d3f::state_t inState, d3f::state_t outState );
      virtual void end();
   private:
      const tkey_t m_key;
      const ids_t m_ids;
      puid_t m_puOwner;
   };

}

