#pragma once

/// The same as '_Pragma( "something" )' that is the same as '#pragma something'
/// The advantage of 'PRAGMA'/'_Pragma' over '#pragma' is that they may be used inside a macro.
/// The advantage of 'PRAGMA' over '_Pragma' is that it cannot need '""'
/// Example: PRAGMA( unroll 4 ) is equivalent to _Pragma( "unroll 4" ) and '#pragma unroll 4'
#ifndef PRAGMA
#  define PRAGMA(X) _Pragma( #X )
#endif

/// The same as _Pragma( "openmp_directive" ) or "#pragma openmp_directive"
/// Example: OPENMP( omp parallel ) is equivalent to _Pragma( "omp parallel" ) and "#pragma omp parallel".
/// The advantage of 'OPENMP' over the 'PRAGMA' is that 'OPENMP' is empty when compiled without OpenMP (avoid warnings)
#if defined(_OPENMP) && !defined(OPENMP)
#  define OPENMP(X) _Pragma( #X )
#else
#  define OPENMP(X)
#endif
