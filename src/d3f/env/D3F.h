#pragma once

// STD
#include <string>

namespace d3f::env {

   struct D3F
   {
      D3F() = delete;

      static std::string GetEnv( const char * envName );
      static const int VERBOSE;
      static const int DBG_LEVEL;

      template<typename F_DEBUG, int LEVEL = 1>
      static inline void Debug( const F_DEBUG &fDebug )
      {
         if ( LEVEL <= DBG_LEVEL )
            fDebuf();
      }

      template<typename F_VERBOSE, int LEVEL = 1>
      static inline void Verbose( const F_VERBOSE &fVerbose )
      {
         if ( LEVEL <= VERBOSE )
            fVerbose();
      }
   };


//   template<typename T1, typename T2, typename F_MSG>
//   static inline void ck_equ( T1 t1, T2 t2, const F_MSG &fMsg )
//   {
//      if ( !( t1 == t2 ) )
//      {
//         throw error( fMsh() );
//      }
//      ck_equ( 0, 1, [](){ return std::string( "" ) + ""; } );
//   }

}

using d3f::env::D3F;
