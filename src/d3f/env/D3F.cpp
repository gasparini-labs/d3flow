// STD
#include <algorithm>
#include <exception>

// Boost
#include <boost/algorithm/string.hpp>

// D3F
#include <d3f/env/D3F.h>

#ifdef NDEBUG
   int const DEFAULT_D3F_VERBOSE = 0;
   int const DEFAULT_D3F_DBG_LEVEL = 0;
#else
   int const DEFAULT_D3F_VERBOSE = 1;
   int const DEFAULT_D3F_DBG_LEVEL = 1;
#endif

namespace d3f::env {

   std::string D3F::GetEnv( const char * envName )
   {
      const char* value = std::getenv( envName );
      return value ? std::string( value ) : std::string( "" );
   }

   static int GET_INT_ENV_VALUE( const char* key, int defaultValue )
   {
      std::string INT_ENV_VALUE( D3F::GetEnv( key ) );
      if ( !INT_ENV_VALUE.empty() )
         return std::max( 0, std::stoi( INT_ENV_VALUE ) );
      else
         return defaultValue;
   }

   static bool GET_BOOL_ENV_VALUE( const char* key, bool defaultValue )
   {
      std::string BOOL_ENV_VALUE( D3F::GetEnv( key ) );
      if ( !BOOL_ENV_VALUE.empty() )
      {
         boost::algorithm::to_lower( BOOL_ENV_VALUE );
         if(
            BOOL_ENV_VALUE == "on"   |
            BOOL_ENV_VALUE == "yes"  |
            BOOL_ENV_VALUE == "true" |
            BOOL_ENV_VALUE == "1"    )
            return true;
         else if (
            BOOL_ENV_VALUE == "off"   |
            BOOL_ENV_VALUE == "no"    |
            BOOL_ENV_VALUE == "false" |
            BOOL_ENV_VALUE == "0"     )
            return false;
         else
         {
            throw std::runtime_error( "Invalid value for the env. var. " + std::string( key ) + ". The valid values are '0|false|no|off' for 'false' and '1|true|yes|on' for 'true'." );
         }
      }
      else
         return defaultValue;
   }

   const int D3F::VERBOSE = GET_INT_ENV_VALUE( "D3F_VERBOSE", DEFAULT_D3F_VERBOSE );
   const int D3F::DBG_LEVEL = GET_INT_ENV_VALUE( "D3F_DBG_LEVEL", DEFAULT_D3F_DBG_LEVEL );

}
