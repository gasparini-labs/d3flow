// D3F
#include <d3f/D3Flow.h>

namespace d3f {

   D3Flow::D3Flow()
   {
   }

   D3Flow::~D3Flow()
   {
   }

   static void Test2()
   {
      D3Flow exe;

//      auto const [ i, j, k ] = "i,j,k"_ids;
//      "A(i,j)"_obj;
//      "A(i,j)*x(j)"_tsk;
//      "A(i,j)|1"_in;
//      "A(i,j)|0"_out;
//      "A(i,j)|0,1"_inout;

//      exe.generator( m_cy, 2, d3f::last_level, [&]( Runtime &rtm, d3f::DataDef &cy, d3f::ids_t ids )
//      {
//         auto const [ i, j ] = ids;
//         auto A = rtm.object( "A" );
//         auto cx = rtm.object( "compressed_x" );
//         auto js = A.domainIndexes( i, std::all );
//
//         rtm.ordered( [&]()
//         {
//            for ( auto j : js )
//               rtm.task( "OffDiagSpMV", [&]( d3f::TaskDef& def )
//               {
//                  def.in( A(i,j) )
//                     .in( cx(i,j) )
//                     .out( y(i)^2 );
//               } );
//         } );
//      } );

      exe.execute( [&]( Runtime &rtm )
      {
//         auto y = rtm.object( "y" );
//         auto A = rtm.object( "A" );
//         auto x = rtm.object( "x" );
//         auto cx = rtm.object( "compressed_x" );

//         rtm.generatorBgn( cy, d3f::lower_level, 1 );
//         auto ids = genIds();
//         for ( auto j : js )
//         {
//            auto def = rtm.task( "" );
//            def.in();
//            def.in();
//            def.out();
//            def.end();
//         }
//         rtm.generatorEnd();

//         rtm.generator( cy, d3f::lower_level, 1, [&]( d3f::genids_t ids )
//         {
//            auto const [ i, j ] = ids;
//            for ( auto j : js )
//               rtm.task( "PartialOffDiagSpMV", [&]( d3f::TaskDef& def )
//               {
//                  def.in( A(i,j) )
//                     .in( cx(i,j) )
//                     .out( cy(i,j)^1 );
//               } );
//         } );

//         rtm.generator( cy, d3f::lower_level, 1, [&]( d3f::ids_t ids )
//         {
//            auto const [ i, j ] = ids;
//            for ( auto j : js )
//               rtm.task( "PartialOffDiagSpMV", [&]( d3f::TaskDef& def )
//               {
//                  def.in( A(i,j) )
//                     .in( cx(i,j) )
//                     .out( cy(i,j)^1 );
//               } );
//         } );
//
//         int i = 0;
//
//         rtm.task( "DiagSpMV", [&]( d3f::TaskDef& def )
//         {
//            def.in( A(i,i) )
//               .in( x(i) )
//               .out( y(i), 1 );
//         } );
//
//         rtm.task( "DiagSpMV" )
//            .in( A(i,i) )
//            .in( x(i) )
//            .out( y(i), 1 )
//            .end();
//
//         auto js = A.domainIndexes( i, std::all );
//
//         rtm.ordered( "OffDiagSpMV" )
//            .in( A(i,js) )
//            .in( cx(i,js) )
//            .out( y(i)^2 )
//            .end();
//
//         rtm.ordered( "OffDiagSpMV", []( d3f::TaskDef& def )
//         {
//            def.in( A(i,js) )
//               .in( cx(i,js) )
//               .out( y(i)^2 );
//         } );
//
//         rtm.ordered( [&]()
//         {
//            for ( auto j : js )
//               rtm.task( "OffDiagSpMV" )
//                  .in( A(i,j) )
//                  .in( cx(i,j) )
//                  .out( y(i)^2 )
//                  .end();
//         } );
//
//         rtm.ordered( [&]()
//         {
//            for ( auto j : js )
//               rtm.task( "OffDiagSpMV", [&]( d3f::TaskDef& def )
//               {
//                  def.in( A(i,j) )
//                     .in( cx(i,j) )
//                     .out( y(i)^2 );
//               } );
//         } );
//
//         rtm.reduce( d3f::ordered, [&]()
//         {
//            for ( auto j : js )
//               rtm.task( "OffDiagSpMV", [&]( d3f::TaskDef& def )
//               {
//                  def.in( A(i,j) )
//                     .in( cx(i,j) )
//                     .out( y(i)^2 );
//               } );
//         } );






//         rtm.ordered( "OffDiagSpMV", js, []( d3f::TaskDef& def, int j )
//         {
//            def.in( A(i,j) )
//               .in( cx(i,j) )
//               .out( y(i)^2 )
//               .end(); // Important
//         } );
//
//         rtm.ordered( "OffDiagSpMV", []( d3f::TaskDef& def )
//         {
//            for ( auto j : js )
//               def.in( A(i,j) )
//                  .in( cx(i,j) )
//                  .out( y(i)^2 )
//                  .end(); // Important
//         } );

//         rtm.task( "OffDiagSpMV", []( d3f::TaskDef& def )
//         {
//            for ( int j : { 0, 1, 2 } )
//               def.in( A(i,j) )
//                  .in( cx(i,j) )
//                  .reduce( y(i), 2 );
//         } );
//
//         rtm.reduce( "OffDiagSpMV", y(i), 2, iCoupled_js, []( d3f::TaskDef& def, d3f::did_t j )
//         {
//            def.in( A(i,j) )
//               .in( cx(i,j) );
//         } );
//
//         rtm.reduce( "OffDiagSpMV", y(i)^2, []( d3f::TaskDef& def )
//         {
//            def.in( A(i,d3f::all) )
//               .in( cx(i,d3f::all) );
//         } );
//
//         rtm.reduce( "OffDiagSpMV", y(i)^2, []( d3f::TaskDef& def )
//         {
//            auto js = d3f::all;
//            def.in( A(i,js) )
//               .in( cx(i,js) );
//         } );
//
//         auto js = A.metadata( "coupled domain columns" )( i );
//         auto js = A.metadata( i, d3f::all );
//         auto js = A.metadata().all( 1 );
//
//         rtm.reduce( "OffDiagSpMV", y(i)^2, []( d3f::TaskDef& def )
//         {
//            def.in( A(i,js) )
//               .in( cx(i,js) );
//         } );
//
//         rtm.ordered( "OffDiagSpMV", []( d3f::TaskDef& def )
//         {
//            def.in( A(i,js) )
//               .in( cx(i,js) )
//               .out( y(i)^2 );
//         } );
//
//         rtm.unordered( "OffDiagSpMV", []( d3f::TaskDef& def )
//         {
//            def.in( A(i,Ai_js) )
//               .in( cx(i,Ai_js) )
//               .out( y(i)^2 );
//         } );

//         rtm.reduce( "OffDiagSpMV", { 0, 1, 2 }, []( int j )
//         {
//
//         } );

/*
         rtm.task( "A.nzCols" )
            .in( "A", {0,0}, 0 )
            .out( "x", {0}, 0 )
            .end();
*/


//         int A, x;
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0)(0,0)[0] )
//            .out( x(0)(0)[0] )
//            .end();
//
//         rtm.update( A(0,0), 1 );
//
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0), 0 )
//            .out( x(0), 0 )
//            .end();
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0)[0] )
//            .out( x(0)[0] )
//            .end();
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0).state(0) )
//            .out( x(0).state(0) )
//            .end();
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0)^0 )
//            .out( x(0)^0 )
//            .end();
//
//         rtm.task( "A.nzCols" )
//            .in( A(0,0)[i]^0 )
//            .out( x(0)[i]^0 )
//            .end();

//         rtm.task( "A*x", [&]( Task &t )
//         {
//            t.in( "A", {0,0}, 0 );
//            t.in( "x", {0}, 0 );
//            t.out( "y", {0}, 1 );
//         } );
//
//         rtm.task( "A*x" )
//            .in( "A", {0,0}, 0 )
//            .in( "x", {0}, 0 )
//            .out( "y", {0}, 1 )
//            .run();
//
//         rtm.task( "A*x" )
//            .in( "A", {0,0}, 0 )
//            .in( "x", {0}, 0 )
//            .out( "y", {0}, 1 )
//            .done();
//
//         rtm.task( "A*x" )
//            .in( "A", {0,0}, 0 )
//            .in( "x", {0}, 0 )
//            .out( "y", {0}, 1 )
//            .end();
//
//         rtm.task( "A*x", d3f::flow()
//                  .in( "A", {0,0}, 0 )
//                  .in( "x", {0}, 0 )
//                  .out( "y", {0}, 1 ) );
//
//         rtm.task( "A*x",
//                  { d3f::in( "A", {0,0}, 0 ),  d3f::in( "x", {0}, 0 ) },
//                  { d3f::out( "y", {0}, 1 ) } );
//
//         rtm.generator( "cx", 1, [&]( int i, int j )
//         {
//            rtm.task( "compr(x)", d3f::flow()
//                     .in( "nzjs(A(i,j))", {i,j}, 0 )
//                     .in( "x(j)", {j}, 0 )
//                     .out( "cx{i,j}", {i,j}, 1 ) );
//         } );
//
//         rtm.generator( "cx", 1, [&]( int i, int j )
//         {
//            rtm.task( "compr(x)" )
//               .in( "nzjs(A(i,j))", {i,j}, 0 )
//               .in( "x(j)", {j}, 0 )
//               .out( "cx{i,j}", {i,j}, 1 )
//               .end();
//         } );
      } );
   }

   //   static void Test()
   //   {
   //      CoarseDataflow cd( std::shared_ptr<ProcessingEnvironment>() );
   //
   //      std::shared_ptr<Object> A, x, cx, b;
   //      std::shared_ptr<Task> comprx, Ax;
   //      std::shared_ptr<Generator> compr_x, Ax;
   //
   //      cd.object( A, "A", "A($0,$1)" )
   //        .object( x, "x", "x($0)" )
   //        .object( cx, "cx", "cx($0,$1)" )
   //        .object( b, "b", "b($0)" )
   //        .task( comprx, "Gen(cx)", "cx($0,$1) = x($1) compressed for A($0,$1)*x($1)" )
   //        .task( Ax );
   //
   //      cd.execute( []( Graph& gph )
   //      {
   //         int i = rank;
   //
   //         gph.generator( "cx", 1, "Compress(x)", [=]( Task& task, ids_t cx_ids )
   //         {
   //             auto const [ i, j ] = cx_ids;
   //             task.in( "x", { i } )
   //                 .in( "A", { i, j } );
   //         } );
   //
   //         gph.task( "A(i,i)*x(i)" )
   //            .in( "A", { i, i } )
   //            .in( "x", { i } )
   //            .out( "b", { i }, 1 );
   //
   //         double norm_b; // Replicated
   //         gph.foreach( 1, maxIter, [=]( int k )
   //         {
   //            gph.reduce( "|b|", {}, k, "Norm{b}" );
   //            gph.foreach( jCoupling, [=]( int j )
   //            {
   //               gph.task( "y(i) += A(i,j)*x(j)" )
   //                  .in( "A", { i, j } )
   //                  .in( "x", { j } )
   //                  .out( "y", { i } );
   //            } );
   //         } );
   //
   //         gph.foreach( 0, 100, 1, []( int j )
   //         {
   //            gph.task( "A(i,j)*cx(i,j)" )
   //               .in( "A", { i, j } )
   //               .in( "cx", { i, j } )
   //               .out( "partial_b", { i, j } );
   //            gph.stop();
   //         } );
   //
   //         gph.foreach( jCoupled, []( int j )
   //         {
   //            gph.task( "A(i,j)*cx(i,j)" )
   //               .in( "A", { i, j } )
   //               .in( "cx", { i, j } )
   //               .out( "partial_b", { i, j } );
   //         } );
   //
   //         gph.reduce( "b", { i }, 2, "Sum(partial_b(i,j))", jCoupled, []( Task &task, int j )
   //         {
   //            task.in( "partial_b", { i, j } );
   //         } );
   //
   //         gph.reduce( "b", { i }, 2, "A(i,j)*cx(i,j)", jCoupled, []( Task &task, int j )
   //         {
   //            task.weigth( 0.5 )
   //               .in( "A", { i, j } )
   //               .in( "cx", { i, j } );
   //         } );
   //
   //         gph.reduce( jCoupled, []( int j )
   //         {
   //            gph.task( "A(i,j)*cx(i,j)" )
   //               .in( "A", { i, j } )
   //               .in( "cx", { i, j } )
   //               .out( "b", { i }, 2 );
   //         } );
   //
   //         gph.reduce( "b", { i }, 2, jCoupled, []( int j )
   //         {
   //            gph.task( "A(i,j)*cx(i,j)" )
   //               .in( "A", { i, j } )
   //               .in( "cx", { i, j } );
   //         } );
   //
   //      } );
   //   }


}
