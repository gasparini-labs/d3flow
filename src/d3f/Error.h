#pragma once

// STD
#include <exception>
#include <stdexcept>
#include <string>

namespace d3f {

   class Error : public std::runtime_error
   {
   public:
      Error();
      Error( const std::string &message );
      Error( const std::exception &except );
      Error( const Error&  e ) = default;
      Error(       Error&& e ) = default;
      virtual ~Error();
      static void Print( const char *msg );
   };

}
