// STD
#include <iostream>

// Boost
#if __has_include( <boost/stacktrace.hpp> )
#include <boost/stacktrace.hpp>
#define D3F_BACKTRACE_ON
#else
#undef D3F_BACKTRACE_ON
#endif

// D3F
#include <d3f/Error.h>

namespace d3f {

   void Error::Print( const char *msg )
   {
#ifdef _OPENMP
#pragma omp critical ( d3f_error )
#endif
      {
         std::cerr.flush();
         std::cerr << "Error message:" << std::endl;
         std::cerr << msg << std::endl;
#ifdef D3F_BACKTRACE_ON
         std::cerr << "Backtrace:" << std::endl;
         std::cerr << boost::stacktrace::stacktrace() << std::endl;
#endif
         std::cerr.flush();
      }
   }

   Error::Error() :
      std::runtime_error( std::string{ "Unknown exception" } )
   {
      Print( what() );
   }

   Error::Error( const std::string &message ) :
      std::runtime_error( message )
   {
      Print( what() );
   }

   Error::Error( const std::exception &except ) :
      std::runtime_error( std::string{  except.what() } )
   {
      Print( what() );
   }

   Error::~Error()
   {
   }

}
